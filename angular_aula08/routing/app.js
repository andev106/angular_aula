const express = require('express')
const app = express()
const path = require('path')

app.use(express.static(path.join(__dirname, 'dist', 'routing')))

app.get('/', (request, response) => {
    return response.sendFile(path.join(__dirname, 'dist', 'routing', 'index.html'))
})

app.listen(4000)