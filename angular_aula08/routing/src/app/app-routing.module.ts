import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes} from "@angular/router"
import { HomeComponent } from "./pages/home/home.component";
import { ErrorComponent } from "./pages/error/error.component";
import { ProductComponent } from "./pages/product/product.component";
import { NewProductComponent } from "./pages/new-product/new-product.component";
import { ProductsComponent } from "./pages/products/products.component";
import { UpdateProductComponent } from "./pages/update-product/update-product.component";
import { FormVerificationGuard } from "./guards/form-verification.guard";
import { AuthVerificationGuard } from "./guards/auth-verification.guard";
import { ChildAuthVerificationGuard } from "./guards/child-auth-verification.guard";

const routes: Routes = [

    {
       path: '',
       pathMatch: 'full',
       redirectTo: '/home'
    },
    {
        path:'home',
        component:HomeComponent,     
        children: [
            {
        path: ':name',
        component: ProductComponent
       }     
       ],
       canActivateChild: [
           ChildAuthVerificationGuard
       ]
    },
    {
        path:'new',
        component: NewProductComponent,
        canDeactivate: [
            FormVerificationGuard
        ],
        canActivate: [
            AuthVerificationGuard
        ]
    },

    {
        path: 'products',
        component:ProductsComponent
    },

    {
        path:'update',
        component: UpdateProductComponent,
        canDeactivate: [
            FormVerificationGuard
        ],
        canActivate: [
            AuthVerificationGuard
        ]
    },

    {
        path:'product/:name',
        component:ProductComponent
    },
    
    {  
    path: '**',
    //redirectTo: '/login'
    component:ErrorComponent
}
]

@NgModule ({
    declarations: [

    ],
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}