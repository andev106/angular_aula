import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Product } from 'src/app/models/product';
import { ProductsApiService } from 'src/app/services/products-api.service'
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productName: string = ''
  prod!: Product


// ?nome=valor&nome=valor
//query param
  
  constructor(
    private route: ActivatedRoute,
    private prodApiService: ProductsApiService,
    private snack: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit(): void {
    //this.productName = this.route.snapshot.paramMap.get('name') || ''
    
    this.route.paramMap.subscribe(
      (paramMap)=> {
      this.productName = paramMap.get('name') || ''

      this.prodApiService.findByProductName(this.productName).subscribe(
        (prod) => {
          this.prod = prod
        }
      )

    } ) 

  }
  delete(id: number): void {
    this.prodApiService.deleteProduct(id).subscribe(()=>{
    this.snack.open('Produto excluido!', 'Ok')
    this.router.navigateByUrl('/home')
    })
  }
  edit(): void {
    this.router.navigateByUrl(`/update?id=${this.prod.id}&name=${this.prod.name}&price=${this.prod.price}&picture=${this.prod.picture}`)
  }
}
